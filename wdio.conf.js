const fs = require('fs');
const CustomReporter = require('./support/testStepLogger');
const path = require('path');
const logger = require('./support/logger');
const multipleCucumberHtmlReporter = require('wdio-multiple-cucumber-html-reporter');
const { promisify } = require('util');


const readDir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);

CustomReporter.reporterName = 'myCustomReporter';

const config = {
  specs: [],
  exclude: [
    // 'path/to/excluded/files'
  ],
  maxInstances: 1,
  capabilities: [{
    browserName: 'chrome',
    metadata: {
      browser: {
        name: 'chrome',
      },
      device: 'Jenkins job - "date"',
      platform: {
        name: 'Windows',
        version: '10'
      }
    },
    loggingPrefs: {
      'driver': 'INFO',
      'browser': 'INFO'
    }
  }],

  sync: true,
  logLevel: 'error',
  coloredLogs: true,
  deprecationWarnings: false,
  bail: 0,
  screenshotPath: './errorShots/',
  waitforTimeout: 30000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  framework: 'cucumber',
  reporters: ['multiple-cucumber-html', CustomReporter],
  reporterOptions: {
    htmlReporter: {
      jsonFolder: './reports/json',
      reportFolder: `./reports/html`,
      displayDuration: true
    }
  },

  cucumberOpts: {
    require: ['./step_definitions/**/*.js', './support/hooks.js'], // <string[]> (file/dir) require files before executing features
    backtrace: false, // <boolean> show full backtrace for errors
    compiler: ['js:babel-register'], // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    dryRun: false, // <boolean> invoke formatters without executing steps
    failFast: false, // <boolean> abort the run on first failure
    format: ['json:./reports/json/cucumber_report.json'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    colors: true, // <boolean> disable colors in formatter output
    snippets: true, // <boolean> hide step definition snippets for pending steps
    source: true, // <boolean> hide source uris
    profile: [], // <string[]> (name) specify the profile to use
    strict: false, // <boolean> fail if there are any undefined or pending steps
    tags: [], // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    timeout: 120000, // <number> timeout for step definitions
    ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings.
  },

  // capabilities
  onPrepare: async config => {
    const allFeatures = path.join(__dirname, '/features/');
    // const jsonPath = path.join(__dirname, '/reports/json');
    // const logPath = path.join(__dirname, '/reports/logs');
    //
    // const featurePath = path.join(__dirname, './features');
    // const stepPath = path.join(__dirname, './step_definitions');
    // const pagePath = path.join(__dirname, './pages');

    try {
      const tag = `@${process.env.npm_lifecycle_event}`;
      const regex = '\\' + 's' + tag + '\\' + 's';
      const tagMatcher = new RegExp(regex);

      const filenames = await readDir(allFeatures);

      for(let filename of filenames) {
        const pathToFeature = path.join(allFeatures, filename);
        const feature = await readFile(pathToFeature, 'utf-8');
        if(tagMatcher.test(feature)) config.specs.push(pathToFeature);
      }
    } catch (e) {
      throw e;
    }
  },

  beforeSession: function () {},

  // before: function (capabilities, specs) {
  // },

  // beforeCommand: function (commandName, args) {
  // },

  // beforeFeature: function (feature) {
  // },

  // beforeScenario: function (scenario) {
  // },

  // beforeStep: function (step) {
  // },

  // afterStep: function (stepResult) {
  // },

  // scenario
  afterScenario: function () {},
  onError: function() {
    // Triggers on a failed step

    /**
     * session - <promise> | resolves =>
     * session - {
          sessionID: <string>,
          value: <array> of logEntries
      }
     * logEntries - {
          level: <string>,
          message: <string>,
          source: <string>,
          timestamp: <number>
     * }
     */

    const session = browser.log('browser'); // Due to it being part of the global browser object, we do not need to explicitly wait for promise to resolve
    const logEntries = session.value;

    if(Array.isArray(logEntries) && logEntries.length !== 0) {
      for(let entry of logEntries) {
        if(entry.level === 'SEVERE') {
          logger.console(entry);

          if(multipleCucumberHtmlReporter) {
            multipleCucumberHtmlReporter.attach(entry);
          }
        }
      }
    }
  },

  // afterFeature: function (feature) {
  // },

  // afterCommand: function (commandName, args, result, error) {
  // },

  // after: async function (result, capabilities, specs) {
  // },

  // afterSession: function (config, capabilities, specs) {
  // },

  // exitCode, config, capabilities
  onComplete: function() {}
};


logger.info('Running tests locally with wdio "seleniun-standalone" service');
config.services = ['selenium-standalone'];


module.exports.config = config;
