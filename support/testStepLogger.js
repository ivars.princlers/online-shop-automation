const util = require('util'),
  events = require('events');
const { logger } = require('./imports');

const CustomReporter = function() {
  let scenarioName = null;
  let skipTest = false;
  
  this.on('suite:start', featureInfo => {
    if(featureInfo.parent === null) {
      logger.info(`Running feature - ${featureInfo.title}`);
    }
  });
  
  this.on('test:start', function(stepToRun) {
    if(scenarioName === null || scenarioName !== stepToRun.scenarioName) {
      logger.info(`Scenario: ${stepToRun.scenarioName}`);
      scenarioName = stepToRun.scenarioName;
      skipTest = false;
    }
    if (!stepToRun.uid.includes('hooks')) {
      (!skipTest) ? logger.info(`STEP: ${stepToRun.title}`) : logger.warn(`STEP SKIPPED: ${stepToRun.title}`);
    }
  });

  this.on('test:fail', test => {
    skipTest = true;
    logger.error(`STEP: ${test.title} FAILED \n ${test.err.stack}`);
  });
};

util.inherits(CustomReporter, events.EventEmitter);
exports = module.exports = CustomReporter;
