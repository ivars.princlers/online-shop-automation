const winston = require('winston');
const { createLogger, format, transports } = require('winston');
const path = require('path');
const fs = require('fs');
/*
Available logLevels to pass to --logLevel=<value>
{ error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
 */
const reportPath = path.join(__dirname, '../reports');

const myCustomLevels = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    verbose: 3,
    console: 4,
    debug: 5
  },
  colors: {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    verbose: 'cyan',
    console: 'magenta',
    debug: 'blue'
  }
};

if(!fs.existsSync(reportPath)) {
  fs.mkdirSync(reportPath, err => {
    if(err) throw err;
  });
}
const opts = format.combine(
  format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  format.printf(({ level, message, timestamp }) => {
    if(typeof message === 'object') return `${timestamp} ${level.toUpperCase()}: |\n${JSON.stringify(message, null, 2)}`;
    return `${timestamp} ${level.toUpperCase()}: | ${message}`;
  })
);

const consoleOpts = { level: process.env.npm_config_logLevel || 'info' };

if(process.env.npm_config_color) consoleOpts.format = format.colorize({ all: true });

winston.addColors(myCustomLevels.colors);

const logger = createLogger({
  levels: myCustomLevels.levels,
  format: opts,
  transports: [
    new transports.Console(consoleOpts),
    new winston.transports.File({
      filename: 'reports/logs/info.log',
      level: 'info'
    }),
    new winston.transports.File({
      filename: 'reports/logs/verbose.log',
      level: 'verbose'
    }),
    new winston.transports.File({
      filename: 'reports/logs/debug.log',
      level: 'debug'
    })
  ]
});

logger.exitOnError = false;
module.exports = logger;
