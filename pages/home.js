import Main from './main';

class Home extends Main {
  constructor() {
    super();
  }

  get homePageTitle() {
    return $('//title[contains(., "My Store")]');
  }
  get homePageSignInButton() {
    return $('//a[contains(@title, "Log in to your customer account")]');
  }
}
export default new Home();
