import Main from './main';

class Order extends Main {
    constructor() {
        super();
        this.orderSize = null;
        this.orderColor = null;
        this.orderQuantity = null;


    }
    get myAccoutnPageSearchInputField(){
        return $('input[name="search_query"]');
    }
    get myAccoutnPageSearchInputFieldButton(){
        return $('button[name="submit_search"]');
    }
    get myAccoutnProductContainer(){
        return browser.elements('//div[contains(@class, "product-container")]');
    }
    get myAccoutnProductImgLink(){
        return $('a[class="product_img_link"]');
    }
    get myAccoutnProductDescription(){
        return $('div[itemprop =  "description"]');
    }
    get myAccoutnProductCondition(){
        return $('//p[@id= "product_condition"]');
    }
    get myAccoutnProductQuantity(){
        return $('//input[@id= "quantity_wanted"]');
    }
    get myAccoutnProductSizeButton(){
        return $('//div[contains(@id, "group_1")]');
    }
    get myAccoutnProductSizeValue(){
        return $(`option[title = "${this.orderSize}"]`);
        //return $(`//div[contains(@id, "group_1")]//option[contains(@title, "${this.orderSize}")]`);
    }
    get myAccoutnProductColor(){
        return $(`//a[contains(@class, "color_pick") and contains(@name, "${this.orderColor}")]`);
    }
    get myAccoutnProductSubmitOrder(){
        return $(`//button[contains(@name, "Submit")]`);
    }
    get myAccoutnProductSuccessfullyAddedToCartMessage(){
        return $(`//i[contains(@class, "icon-ok")]`);
    }
    get myAccoutnProductProdceedToCheckoutButton(){
        return $(`//a[contains(@title, "Proceed to checkout")]`);
    }
    get myAccoutnProductShoppingCartTitlen(){
        return $(`//h1[contains(@id, "cart_title")]`);
    }

    get myAccoutnProductCartQuantity(){
        return $(`//input[contains(@class, "cart_quantity_input")]`);
    }
    get myAccoutnProductCartProceedToCheckOut(){
        return $(`//a[contains(@title, "Proceed to checkout")and contains(@class, "standar")]`);
    }

    setOrderSize(size){
        this.orderSize = size;
    }
    setOrderColor(color) {
        this.orderColor = color;
    }
    setOrderQuantity(quant) {
        this.orderQuantity = quant;
    }
}
export default new Order();
