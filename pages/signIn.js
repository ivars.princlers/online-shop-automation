import Main from './main';

class SignIn extends Main {
  constructor() {
    super();
    this.coinFlip = (Math.floor(Math.random() * 2) === 0);
    this.uniqueId = Math.random().toString(36).substr(2, 9);

    this.user = {
      gender: this.coinFlip === 1,
      userEmail: this.gender === true ? `johnDoe${this.uniqueId}@gmail.com` : `janeDoe${this.uniqueId}@gmail.com`,
      userPassword: this.gender === true ? `Dog123456789` : `Cat123456789`,
      userName: this.gender === true ? `John` : `Jane`,
      userLastName: 'Doe',
      userDateOfBirth: this.gender === true ? '22.1.1973' : '23.1.1973',
      userCompany: this.gender !== true,
      userAddress1: '711-2880 Nulla St.',
      userAddress2: this.gender !== true,
      userCity: 'New York',
      userState: 'Alabama',
      userCountry: 'United States',
      userZipCode: '35004',
      userAdditionalInformation: this.gender !== true,
      userHomePhone: this.gender !== true,
      userMobilePhone: '+1-541-754-3011',
      userAddressalias: this.gender !== true,
      newsletter: this.gender !== true,
      specialOffers: this.gender !== true,
    }
  }


  get signInPageTitle() {
    return $('//title[contains(., "Login - My Store")]');
  }
  get signInPageSubmitLoginButton() {
    return $('//button[contains(@name, "SubmitLogin")]');
  }
  get signInPageSubmitCreateAccount() {
    return $('//button[contains(@name, "SubmitCreate")]');
  }
  get signInPageRegisterEmailInputField() {
    return $('//input[contains(@name, "email_create")]');
  }
  get signInPageRegistrationInputFieldGenderTagM() {
    return $('//div[contains(@class, "radio-inlin")][1]');
  }
  get signInPageRegistrationInputFieldGenderTagF() {
    return $('//div[contains(@class, "radio-inlin")][2]');
  }
  get signInPageRegisterNameInputField() {
    return $('//input[contains(@name, "customer_firstname")]');
  }
  get signInPageRegisterLastNameInputField() {
    return $('//input[contains(@name, "customer_lastname")]');
  }
  get signInPageRegisterPasswordInputField() {
    return $('//input[contains(@name, "passwd")]');
  }
  get signInPageRegisterDaysInputFieldButton() {
    return $(`//div[contains(@id, "uniform-days")]`);
  }
  get signInPageRegisterDaysInputFieldNumber() {
    return $(`//div[contains(@id, "uniform-days")]//option[contains(@value,"${this.user.userDateOfBirth.substring(0, 2)}")]`);
  }
  get signInPageRegisterMonthInputFieldButton() {
    return $(`//div[contains(@id, "uniform-month")]`);
  }
  get signInPageRegisterMonthInputFieldNumber() {
    return $(`//div[contains(@id, "uniform-month")]//option[contains(@value,"${this.user.userDateOfBirth[3]}")]`);
  }
  get signInPageRegisterYearInputFieldButton() {
    return $(`//div[contains(@id, "uniform-year")]`);
  }
  get signInPageRegisterYearInputFieldNumber() {
    return $(`//div[contains(@id, "uniform-year")]//option[contains(@value,"${this.user.userDateOfBirth.substring(5, 8)}")]`);
  }
  get signInPageRegisterAddressCompanyInputField() {
    return $('//input[@id="company"]');
  }
  get signInPageRegisterAddress1InputField() {
    return $('//input[@id="address1"]');
  }
  get signInPageRegisterCityInputField() {
    return $('//input[@id="city"]');
  }
  get signInPageRegisterStateInputFieldButton() {
    return $(`//div[contains(@id,"uniform-id_state")]`);
  }
  get signInPageRegisterStateInputFieldValue() {
    return $(`//div[contains(@id,"uniform-id_state")]//option[contains(., "${this.user.userState}")]`);
  }
  get signInPageRegisterZipInputFieldButton() {
    return $(`//input[contains(@id,"postcode")]`);
  }
  get signInPageRegisterCountryInputFieldButton() {
    return $(`//div[contains(@id,"id_country")]`);
  }
  get signInPageRegisterCountryInputFieldValue() {
    return $(`//div[contains(@id,"id_country")]//option[contains(., "${this.user.userCountry}")]`);
  }
  get signInPageRegisterPhone2InputField() {
    return $(`//input[@id = "phone_mobile"]`);
  }
  get signInPageRegisterButton() {
    return $(`//button[contains(@name, "submitAccount")]`);
  }
  get myAccoutnPageTitle() {
    return $('//title[contains(., "My account - My Store")]');
  }

  get myAccoutnPageUserName() {
    let userFullname = this.user.userName + ' ' + this.user.userLastName;
    return $(`//span[contains(.,"${userFullname}")]`);
  }
  get myAccoutnPagePersonalInformationButton() {
    return $(`//span[contains(., "My personal information")]`);
  }
  get myAccoutnPersonalInformationPageTitle() {
    return $('//title[contains(., "Identity - My Store")]');
  }
  get myAccoutnPersonalInformationh1() {
    return $('//h1[contains(., "Your personal information")]');
  }
  get myAccoutnPersonalInformationGenderTag() {
    return $('//span[@class = "checked"]//input');
  }
  get myAccoutnPersonalInformationGenderTag() {
    return $('//span[@class = "checked"]//input');
  }
  get myAccoutnPersonalInformationNameField() {
    return $('//input [contains(@id,"firstname")]');
  }
  get myAccoutnPersonalInformationLastNameField() {
    return $('//input [contains(@id,"lastname")]');
  }
  get myAccoutnPersonalInformationEmailField() {
    return $('//input [contains(@id,"email")]');
  }
  get myAccoutnPersonalInformationDaysField() {
    return $('//select[contains(@name,"days")]//option[@selected = "selected"]');
  }
  get myAccoutnPersonalInformationMonthField() {
    return $('//select[contains(@name,"month")]//option[@selected = "selected"]');
  }
  get myAccoutnPersonalInformationYearField() {
    return $('//select[contains(@name,"year")]//option[@selected = "selected"]');
  }

  get getUser(){
    return this.user;
  }

}
export default new SignIn();
