export default class Main {
  constructor() {
  }
  openUrl(url) {
    browser.url(url);
  }
  executeScript(script) {
    browser.execute(script);
  }
  waitUntilTextMatch(timeoutMS, message, intervalMS, textToMatch, ele) {
    browser.waitUntil(() => {
      return ele.getText().includes(textToMatch);
    }, timeoutMS, message, intervalMS);
  }
  waitUntilVisible(timeoutMS, message, intervalMS, ele) {
    browser.waitUntil(() => {
      return ele.isVisible();
    }, timeoutMS, message, intervalMS);
  }
  waitUntilNewWindowOpens(timeoutMS, message, intervalMS) {
    browser.waitUntil(
      () => browser.windowHandles().value.length === 2, timeoutMS, message, intervalMS
    );
  }
  waitUntilNewWindowCloses(timeoutMS, message, intervalMS) {
    browser.waitUntil(
      () => browser.windowHandles().value.length === 1, timeoutMS, message, intervalMS
    );
  }
  focusOnWindow(window = 0) {
    const windowId = browser.windowHandles().value[window];
    browser.window(`${windowId}`);
  }
  waitForTitle(timeoutMS, message, intervalMS, textToMatch) {
    browser.waitUntil(() => {
      return browser.getTitle() === textToMatch;
    }, timeoutMS, message, intervalMS);
  }
  forceSync(method) {
    browser.call(() => {
      return method;
    });
  }
  getText(element) {
    return browser.getText(element);
  }
  checkUrl(url) {
    return browser.getUrl() === url;
  }
  isEnabled(element) {
    return browser.isEnabled(element) === true;
  }
  setValue(element, text) {
    return browser.setValue(element, text);
  }
  checkIfElementNotVisible(ele, timeout, message) {
    return browser.waitUntil(() => {
      return !ele.isVisible();
    }, timeout, message);
  }
  get baseUrl() {
    const URL = browser.getUrl();
    const BASE_URL = URL.match(/^https?:\/\/[\w\-.]+/);
    return BASE_URL.length ? BASE_URL[0] : undefined;
  }
}
