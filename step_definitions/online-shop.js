import { Given, When, Then, } from 'cucumber';
import home from '../pages/home';
import signIn from '../pages/signIn';
import assert from 'assert';

Given(/^[a-zA-z]+ am in Sign In page$/, () => {
  home.openUrl('http://automationpractice.com/index.php');

  home.homePageTitle.waitForExist(10000);
  home.waitUntilVisible(10000,
    'Sign in button is not visible',
    500,
    home.homePageSignInButton);

  home.homePageSignInButton.click();

  signIn.signInPageTitle.waitForExist(10000);
  signIn.waitUntilVisible(10000,
    'Submit sign in button is not visible',
    500,
    signIn.signInPageSubmitLoginButton);
  signIn.waitUntilVisible(10000,
    'Submit sign in button is not visible',
    500,
    signIn.signInPageRegisterEmailInputField);
});

When(/^[a-zA-z]+ enter email in Create New Account section$/, () => {
  signIn.signInPageRegisterEmailInputField.setValue(signIn.getUser.userEmail);
  signIn.signInPageSubmitCreateAccount.click();
});

When(/^[a-zA-z]+ enter email again in Create New Account section$/, () => {
  signIn.signInPageRegisterEmailInputField.setValue("new" + signIn.getUser.userEmail);
  signIn.signInPageSubmitCreateAccount.click();
});

Then(/^[a-zA-z]+ enter valid account details$/, () => {
  signIn.waitUntilVisible(10000,
    'There is no registration input field canvas visible',
    500,
    signIn.signInPageRegistrationInputFieldGenderTagM);

  if(signIn.getUser.gender) {
      signIn.signInPageRegistrationInputFieldGenderTagM.click();
  } else {
    signIn.signInPageRegistrationInputFieldGenderTagF.click();
  }

  signIn.signInPageRegisterNameInputField.setValue(signIn.getUser.userName);
  signIn.signInPageRegisterLastNameInputField.setValue(signIn.getUser.userLastName);

  signIn.signInPageRegisterPasswordInputField.setValue(signIn.getUser.userPassword);

  signIn.waitUntilVisible(10000,
    'There is no birth day input field field',
    500,
    signIn.signInPageRegisterDaysInputFieldButton);

  signIn.signInPageRegisterDaysInputFieldButton.click();
  signIn.signInPageRegisterDaysInputFieldNumber.click();

  signIn.signInPageRegisterMonthInputFieldButton.click();
  signIn.signInPageRegisterMonthInputFieldNumber.click();

  signIn.signInPageRegisterYearInputFieldButton.click();
  signIn.signInPageRegisterYearInputFieldNumber.click();

  signIn.signInPageRegisterAddress1InputField.setValue(signIn.getUser.userAddress1);
  signIn.signInPageRegisterCityInputField.setValue(signIn.getUser.userCity);

  signIn.signInPageRegisterStateInputFieldButton.click();
  signIn.signInPageRegisterStateInputFieldValue.click();

  signIn.signInPageRegisterZipInputFieldButton.setValue(signIn.getUser.userZipCode);

  signIn.signInPageRegisterCountryInputFieldButton.click();
  signIn.signInPageRegisterCountryInputFieldValue.click();

  signIn.signInPageRegisterPhone2InputField.setValue(signIn.getUser.userMobilePhone);
});



Then(/^Click on Register button$/, () => {
  signIn.waitUntilVisible(10000,
    'There is no submit registration button',
    500,
    signIn.signInPageRegisterButton);
    signIn.signInPageRegisterButton.click();
});

Then(/^[a-zA-z]+ Account page is opened$/, () => {
  signIn.myAccoutnPageTitle.waitForExist(10000);
  signIn.waitUntilVisible(10000,
    'The account name is not vissible in login page',
    500,
    signIn.myAccoutnPageUserName);
});

Then(/^[a-zA-z]+ click on My Personal Information button$/, () => {
  signIn.waitUntilVisible(10000,
    'The account personal button is not vissible in login page',
    500,
    signIn.myAccoutnPagePersonalInformationButton);
  signIn.myAccoutnPagePersonalInformationButton.click();
});

Then(/^[a-zA-z]+ Personal Information page is opened$/, () => {
  signIn.myAccoutnPersonalInformationPageTitle.waitForExist(10000);
  signIn.myAccoutnPersonalInformationh1.waitForExist(10000);
});

Then(/^correct personal information is displayed$/, () => {
  if(signIn.getUser.coinFlip){
    assert(signIn.myAccoutnPersonalInformationGenderTag.getValue(),'1');
  }else{
    assert(signIn.myAccoutnPersonalInformationGenderTag.getValue(),'2');
  }
  assert(signIn.myAccoutnPersonalInformationNameField.getValue(), signIn.getUser.userName);
  assert(signIn.myAccoutnPersonalInformationEmailField.getValue(), signIn.getUser.userEmail);
  assert(signIn.myAccoutnPersonalInformationLastNameField.getValue(), signIn.getUser.userLastName);
  assert(signIn.myAccoutnPersonalInformationDaysField.getValue(), signIn.getUser.userDateOfBirth.substring(0, 2));
  assert(signIn.myAccoutnPersonalInformationMonthField.getValue(), signIn.getUser.userDateOfBirth[3]);
  assert(signIn.myAccoutnPersonalInformationYearField.getValue(), signIn.getUser.userDateOfBirth.substring(5, 8));
});


