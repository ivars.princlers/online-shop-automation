import { Given, When, Then, } from 'cucumber';
import order from '../pages/order';
import assert from 'assert';
import home from "../pages/home";

When(/^[a-zA-z]+ select Search menu$/, () => {
    order.waitUntilVisible(10000,
        'Search field is not vissible in login page',
        500,
        order.myAccoutnPageSearchInputField);
    order.myAccoutnPageSearchInputField.click();
});

When(/^[a-zA-z]+ enter "(.*?)"$/, (blouse) => {
    order.myAccoutnPageSearchInputField.setValue(blouse);
});
When(/^[a-zA-z]+ click on Search button$/, () => {
    order.myAccoutnPageSearchInputFieldButton.click();
});

When(/^only 1 result is found$/, () => {
    order.waitUntilVisible(10000,
        'There is no search results visible ',
        500,
        order.myAccoutnProductContainer);

    let resultCount = order.myAccoutnProductContainer.value.length;
    assert(resultCount, 1)
});

When(/^[a-zA-z]+ select the item$/, () => {
    home.openUrl(order.myAccoutnProductImgLink.getAttribute('href'));
});

When(/^item description and condition is displayed/, () => {
    order.waitUntilVisible(10000,
        'There is no products condition visible ',
        500,
        order.myAccoutnProductCondition);

    order.waitUntilVisible(10000,
        'There is no products description visible ',
        500,
        order.myAccoutnProductDescription);
});

When(/^[a-zA-z]+ change quantity to "(.*?)"$/, (count) => {
    order.setOrderQuantity(count);
    order.myAccoutnProductQuantity.setValue(count);
});

When(/^[a-zA-z]+ set size to "(.*?)"$/, (size) => {
    order.setOrderSize(size);
    order.myAccoutnProductSizeButton.click();
    order.myAccoutnProductSizeValue.click();
});

When(/^[a-zA-z]+ set color to "(.*?)"$/, (color) => {
    order.setOrderColor(color);
    order.myAccoutnProductColor.click();
});

When(/^[a-zA-z]+ select Add to cart button$/, () => {
    order.myAccoutnProductSubmitOrder.click();
});

When(/^item is successfully added to cart$/, () => {
    order.waitUntilVisible(10000,
        'There is no message visible ',
        500,
        order.myAccoutnProductSuccessfullyAddedToCartMessage);
});

When(/^[a-zA-z]+ click on Proceed to checkout button$/, () => {
    order.myAccoutnProductProdceedToCheckoutButton.click()
});

When(/^Shopping cart summary page is opened$/, () => {
    order.waitUntilVisible(10000,
        'There is no shopping cart page visible ',
        500,
        order.myAccoutnProductShoppingCartTitlen);
});

When(/^amount is correctly calculated$/, () => {
    assert(order.myAccoutnProductCartQuantity.getValue(), order.orderQuantity)
});

When(/^Proceed to checkout button is visible$/, () => {
    order.waitUntilVisible(10000,
        'There is no proceed to checkout button visible ',
        500,
        order.myAccoutnProductCartProceedToCheckOut);
});

